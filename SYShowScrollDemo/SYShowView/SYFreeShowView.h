//
//  SYFreeShowView.h
//  SYShowScrollDemo
//
//  Created by lz on 15/12/3.
//  Copyright © 2015年 SY. All rights reserved.
//

#import "SYShowView.h"

@interface SYFreeShowView : SYShowView
-(void)addImages:(NSArray *)images autoShow:(BOOL)isAutoShow andShowInterval:(NSTimeInterval)timeInterval showPage:(BOOL)isPageShow;

-(void)addImages:(NSArray *)images autoShow:(BOOL)isAutoShow andShowInterval:(NSTimeInterval)timeInterval showPage:(BOOL)isPageShow withPageControl:(UIPageControl *)pageControl;
@end
