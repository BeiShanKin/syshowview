//
//  SYFreeShowView.m
//  SYShowScrollDemo
//
//  Created by lz on 15/12/3.
//  Copyright © 2015年 SY. All rights reserved.
//

#import "SYFreeShowView.h"
@interface SYFreeShowView ()
@property (assign,nonatomic) NSTimeInterval timeInterval;
@end

@implementation SYFreeShowView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.timeInterval = 2.0;
    }
    return self;
}

- (void)addImages:(NSArray *)images autoShow:(BOOL)isAutoShow andShowInterval:(NSTimeInterval)timeInterval showPage:(BOOL)isPageShow
{
    [self addImages:images autoShow:NO showPage:isPageShow];
    self.timeInterval = timeInterval;
    if (isAutoShow) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeInterval target:self selector:@selector(autoShow) userInfo:nil repeats:YES];
    }
}

- (void)addImages:(NSArray *)images autoShow:(BOOL)isAutoShow andShowInterval:(NSTimeInterval)timeInterval showPage:(BOOL)isPageShow withPageControl:(UIPageControl *)pageControl
{
    [self addImages:images autoShow:isAutoShow andShowInterval:timeInterval showPage:isPageShow];
    [self.pageControl removeFromSuperview];
    self.pageControl = nil;
    [self addSubview:pageControl];
    self.pageControl = pageControl;
}

@end
