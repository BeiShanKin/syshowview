//
//  SYShowView.m
//  SYShowScrollDemo
//
//  Created by lz on 15/12/3.
//  Copyright © 2015年 SY. All rights reserved.
//

#import "SYShowView.h"

@implementation SYShowView
#pragma mark - 懒加载
- (UIPageControl *)pageControl
{
    if (!_pageControl) {
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height * 0.86, self.frame.size.width, 20)];
        _pageControl = pageControl;
        [_pageControl setPageIndicatorTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        _pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:36.0/255 green:176.0/255 blue:249.0/255 alpha:1];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.currentPage = self.selectedImage;
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

#pragma mark - 初始化

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        //创建scrollView
        UIScrollView *showScrollView = [[UIScrollView alloc] init];
        [self addSubview:showScrollView];
        self.showScrollView = showScrollView;
        self.showScrollView.delegate = self;
        //设置初始属性
        self.selectedImage = 0;
        self.autoShow = NO;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        
        UIScrollView *showScrollView = [[UIScrollView alloc] init];
        [self addSubview:showScrollView];
        self.showScrollView = showScrollView;
        self.showScrollView.delegate = self;
        
        self.selectedImage = 0;
        self.autoShow = NO;
    }
    return self;
}

#pragma mark - 布局相关
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.showScrollView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    self.showScrollView.contentOffset = CGPointMake(self.bounds.size.width, 0);
    self.showScrollView.contentSize = CGSizeMake(self.bounds.size.width * 3, self.bounds.size.height);
    self.showScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.currentView.frame = CGRectMake(self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height);
    self.frontView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    self.afterView.frame = CGRectMake(self.bounds.size.width * 2, 0, self.bounds.size.width, self.bounds.size.height);
}

#pragma mark - ScrollViewDelegate方法

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self refreshImages];
}

#pragma mark - 内部方法
/**
 *  初始化scrollView属性
 */
- (void)setUpShowScrollView
{
    self.showScrollView.showsHorizontalScrollIndicator = NO;
    self.showScrollView.pagingEnabled = YES;
    self.showScrollView.bounces = NO;
    
    UIImageView *frontView = [[UIImageView alloc] init];
    _frontView = frontView;
    [self.showScrollView addSubview:self.frontView];
    
    UIImageView *currentView = [[UIImageView alloc] init];
    _currentView = currentView;
    [self.showScrollView addSubview:self.currentView];
    
    UIImageView *afterView = [[UIImageView alloc] init];
    _afterView = afterView;
    [self.showScrollView addSubview:self.afterView];
}

- (void)imageViewUpdate
{
    //设置当前View
    UIImage *image = self.images[self.selectedImage];
    self.currentView.image = image;
    
    //设置后一个View
    if(self.selectedImage == self.images.count - 1) {
        image = self.images[0];
    } else {
        image = self.images[self.selectedImage + 1];
    }
    self.afterView.image = image;
    //设置前一个View
    if (self.selectedImage == 0) {
        image = self.images.lastObject;
    } else {
        image = self.images[self.selectedImage - 1];
    }
    self.frontView.image = image;
}

/**
 *  刷新图片显示
 */
- (void)refreshImages
{
    if (self.showScrollView.contentOffset.x == 0) {
        if (self.selectedImage == 0) {
            self.selectedImage = self.images.count - 1;
        } else {
            self.selectedImage -= 1;
        }
    } else if (self.showScrollView.contentOffset.x == self.bounds.size.width * 2){
        if (self.selectedImage + 1== self.images.count) {
            self.selectedImage = 0;
        } else {
            self.selectedImage += 1;
        }
    } else {
        return;
    }
    [self.showScrollView setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
    [self imageViewUpdate];
    self.pageControl.currentPage = self.selectedImage;
}

/**
 *  自动轮播功能
 */
- (void)autoShow
{
    
    [UIView animateWithDuration:0.5 animations:^{
        self.showScrollView.contentOffset = CGPointMake(self.frame.size.width * 2, 0);
    } completion:^(BOOL finished) {
        if (self.images.count > 0) {
            [self refreshImages];
        }
    }];
}

#pragma mark - 外部接口方法
/**
 *  添加显示图片
 *
 *  @param images 装着图片的数组
 */
-(void)addImages:(NSArray *)images autoShow:(BOOL)isAutoShow showPage:(BOOL)isPageShow
{
    self.images = images;
    [self setUpShowScrollView];
    [self imageViewUpdate];
    if (isAutoShow) {
        self.autoShow = isAutoShow;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(autoShow) userInfo:nil repeats:YES];
    }
    if (isPageShow) {
        self.pageControl.hidden = isPageShow;
        if (self.images.count > 0) {
            _pageControl.numberOfPages = self.images.count;
        }
    }
}

#pragma mark - 销毁
-(void)dealloc
{
    if (self.isAutoShow) {
        [self.timer invalidate];
    }
}

@end
