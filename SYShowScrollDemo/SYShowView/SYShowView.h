//
//  SYShowView.h
//  SYShowScrollDemo
//
//  Created by lz on 15/12/3.
//  Copyright © 2015年 SY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYShowView : UIView <UIScrollViewDelegate>

@property (weak, nonatomic) NSTimer *timer;

@property (copy, nonatomic) NSArray *images;
@property (assign, nonatomic) NSUInteger selectedImage;
@property (assign, nonatomic,getter=isAutoShow) BOOL autoShow;

@property (weak, nonatomic) UIScrollView *showScrollView;
@property (weak, nonatomic) UIPageControl *pageControl;
@property (weak, nonatomic) UIImageView *frontView;
@property (weak, nonatomic) UIImageView *currentView;
@property (weak, nonatomic) UIImageView *afterView;

/**
 *  添加显示图片
 *
 *  @param images 装着图片的数组
 */
-(void)addImages:(NSArray *)images autoShow:(BOOL)isAutoShow showPage:(BOOL)isPageShow;

/**
 *  初始化控件
 */
- (void)setUpShowScrollView;
/**
 *  初始化图片显示
 */
- (void)imageViewUpdate;

/**
 *  刷新图片显示
 */
- (void)refreshImages;
/**
 *  自动显示方法
 */
- (void)autoShow;
@end
