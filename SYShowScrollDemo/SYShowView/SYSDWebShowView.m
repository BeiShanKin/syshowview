//
//  SYSDWebShowView.m
//  SYShowScrollDemo
//
//  Created by lz on 15/12/4.
//  Copyright © 2015年 SY. All rights reserved.
//

#import "SYSDWebShowView.h"
#import "UIImageView+WebCache.h"

@implementation SYSDWebShowView

- (void)imageViewUpdate
{
    //设置当前View
    [self.currentView sd_setImageWithURL:[NSURL URLWithString:self.images[self.selectedImage]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
    //设置后一个View
    if(self.selectedImage == self.images.count - 1){
        [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.images.firstObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
    } else {
        [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.images[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
    }
    //设置前一个View
    if (self.selectedImage == 0) {
        [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.images.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
    } else {
        [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.images[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
    }
}

@end
