//
//  ImageScrollView.m
//  1.ScrollViewPropertyDemo
//
//  Created by WY on 13-2-28.
//  Copyright (c) 2013年 puke. All rights reserved.
//

#import "ImageScrollView.h"

@implementation ImageScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.zoom = NO;
        NSLog(@"初始化成功");
        // 设置scrollView的属性
        self.maximumZoomScale = 2;
//        self.minimumZoomScale = 1;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.bounces = NO;
        
        // 添加图片
        _imageView = [[UIImageView alloc] init];
        [self addSubview:_imageView];
        
        // 设置代理
        self.delegate = self;
        
        // 添加单击事件
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap)];
        singleTap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:singleTap];
        
        // 添加双击事件
        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        doubleTap.numberOfTapsRequired = 2;
        self.doubleTap = doubleTap;
        [self addGestureRecognizer:doubleTap];
        
        [singleTap requireGestureRecognizerToFail:doubleTap];
    }
    return self;
}

#pragma mark - UIScrollView Delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    if (!self.isZoom) {
        return nil;
    }
    return _imageView;
}

#pragma mark - Target Action
-(void)handleSingleTap
{
    NSLog(@"ImageScrollView handleSingleTap");
    
    if ([self.imageDelegate respondsToSelector:@selector(imageScrollSingleClick)]) {
        [self.imageDelegate imageScrollSingleClick];
    }
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)tapGesture
{
    if (self.zoomScale >= 2) {
        [self setZoomScale:1 animated:YES];
    }else {
        CGPoint point = [tapGesture locationInView:self];
        [self zoomToRect:CGRectMake(point.x - 40, point.y - 40, 80, 80) animated:YES];
    }
}
@end
