//
//  ImageScrollView.h
//  1.ScrollViewPropertyDemo
//
//  Created by WY on 13-2-28.
//  Copyright (c) 2013年 puke. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ImageScrollViewDelegate <NSObject>

-(void)imageScrollSingleClick;

@end

@interface ImageScrollView : UIScrollView <UIScrollViewDelegate>
{
@private
    UIImageView *_imageView;
}
@property (assign,nonatomic,getter=isZoom) BOOL zoom;
@property (weak, nonatomic) UITapGestureRecognizer *doubleTap;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, weak) id<ImageScrollViewDelegate>imageDelegate;

//- (id)initWithFrame:(CGRect)frame withImageFrame:(CGRect)imageFrame withImage:(UIImage *)image;

@end
