//  轮播器
//  SYPageScroll.m
//  ChinaComic
//
//  Created by lz on 15/9/15.
//  Copyright (c) 2015年 SY. All rights reserved.
//
#import "SYPageScroll.h"
#import "ImageScrollView.h"
#import "SDWebImage/UIImageView+WebCache.h"

#define ZOOM_NUM 2
#define kScreenW self.bounds.size.width
#define kScreenW self.bounds.size.height

@interface SYPageScroll()
@property (strong,nonatomic) NSArray *images;
@property (strong,nonatomic) NSArray *URLArray;
@property (assign,nonatomic,getter=isPage) BOOL page;
@property (assign,nonatomic,getter=isAutoShow) BOOL autoShow;
@property (assign,nonatomic,getter=isLeft) BOOL left;
@property (assign,nonatomic,getter=isUp) BOOL up;
@property (assign,nonatomic,getter=isLoop) BOOL loop;
@property (strong,nonatomic) NSTimer *timer;


@property (weak,nonatomic) UIScrollView *bigScrollView;

//@property (weak,nonatomic) ImageScrollView *frontView;
//@property (weak,nonatomic) ImageScrollView *currentView;
//@property (weak,nonatomic) ImageScrollView *afterView;

@property (weak,nonatomic) UIImageView *frontView;
@property (weak,nonatomic) UIImageView *currentView;
@property (weak,nonatomic) UIImageView *afterView;
@property (weak,nonatomic) UIPageControl *pageControl;
@property (weak,nonatomic) UITapGestureRecognizer *tap;
@end

@implementation SYPageScroll

#pragma mark - 懒加载
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.showsHorizontalScrollIndicator = NO;
        self.bounces = NO;
        self.pagingEnabled = YES;
        self.page = YES;
        self.autoShow = NO;
        self.selectedImage = 0;
        self.left = NO;
        self.up = NO;
        self.loop = YES;
        self.zoom = NO;
        self.fit = NO;
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.showsHorizontalScrollIndicator = NO;
        self.bounces = NO;
        self.pagingEnabled = YES;
        self.page = YES;
        self.autoShow = NO;
        self.selectedImage = 0;
        self.left = NO;
        self.up = NO;
        self.loop = YES;
        self.zoom = NO;
        self.fit = NO;
    }
    return self;
}

- (void)setSelectedImage:(NSUInteger)selectedImage
{
    _selectedImage = selectedImage;
}

//- (ImageScrollView *)frontView
//{
//    if (!_frontView) {
//        ImageScrollView *frontView = [[ImageScrollView alloc] init];
//        
//        frontView.imageView.backgroundColor = [UIColor blackColor];
//        
//        _frontView = frontView;
//        [self addSubview:self.frontView];
//        if (self.isUp) {
//            _frontView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//        } else {
//            _frontView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//        }
//        _frontView.imageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//
//        if (self.isFit) {
//            _frontView.imageView.contentMode = UIViewContentModeScaleAspectFit;
//        }
//        _frontView.zoom = self.isZoom;
//        _frontView.doubleTap.enabled = self.isZoom;
//    }
//    return _frontView;
//}
//
//-(ImageScrollView *)currentView
//{
//    if (!_currentView) {
//        ImageScrollView *currentView = [[ImageScrollView alloc] init];
//        
//        currentView.imageView.backgroundColor = [UIColor blackColor];
//        
//        _currentView = currentView;
//        [self addSubview:self.currentView];
//        if (self.isUp) {
//            _currentView.frame = CGRectMake(0, self.bounds.size.height, self.bounds.size.width, self.bounds.size.height);
//        } else {
//            _currentView.frame = CGRectMake(self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height);
//        }
//        _currentView.imageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//        _currentView.imageDelegate = self;
//        
//        if (self.isFit) {
//            _currentView.imageView.contentMode = UIViewContentModeScaleAspectFit;
//        }
//        
//        _currentView.zoom = self.isZoom;
//        _currentView.doubleTap.enabled = self.isZoom;
//    }
//    return _currentView;
//}
//
//- (ImageScrollView *)afterView
//{
//    if (!_afterView) {
//        ImageScrollView *afterView = [[ImageScrollView alloc] init];
//        
//        afterView.imageView.backgroundColor = [UIColor blackColor];
//        
//        _afterView = afterView;
//        [self addSubview:self.afterView];
//        if (self.isUp) {
//            _afterView.frame = CGRectMake(0, self.bounds.size.height * 2, self.bounds.size.width, self.bounds.size.height);
//        } else {
//            _afterView.frame = CGRectMake(self.bounds.size.width * 2, 0, self.bounds.size.width, self.bounds.size.height);
//        }
//        _afterView.imageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//        
//        if (self.isFit) {
//            _afterView.imageView.contentMode = UIViewContentModeScaleAspectFit;
//        }
//        
//        _afterView.zoom = self.isZoom;
//        _afterView.doubleTap.enabled = self.isZoom;
//    }
//    return _afterView;
//}

-(UIPageControl *)pageControl
{
    if (!_pageControl) {
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height * 0.86, kScreenW, 20)];
//        CGRect frame = [self.superview convertRect:_pageControl.frame fromView:self];
//        pageControl.frame = frame;
//        NSLog(@"-------%@-------",NSStringFromCGRect(frame));
        _pageControl = pageControl;
        if (self.images.count > 0) {
            _pageControl.numberOfPages = self.images.count;
        } else {
            _pageControl.numberOfPages = self.URLArray.count;
        }
        [_pageControl setPageIndicatorTintColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.4]];
        _pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:36.0/255 green:176.0/255 blue:249.0/255 alpha:1];
        _pageControl.hidesForSinglePage = YES;
        _pageControl.currentPage = self.selectedImage;
        [self.superview addSubview:_pageControl];
    }
    return _pageControl;
}

- (UITapGestureRecognizer *)tap
{
    if (!_tap) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClick:)];
        _tap = tap;
        [self addGestureRecognizer:tap];
    }
    return  _tap;
}

#pragma mark - 外部接口方法
/**
 *  添加显示图片
 *
 *  @param images 装着图片的数组
 */
-(void)addImages:(NSArray *)images
{
    self.images = images;
    [self scrollUpdate];
    [self imageViewUpdate];
}
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page
{
    [self addImages:images];
    self.pageControl.hidden = !page;
}
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param clickEnable 图片是否可以点击
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page witnImageClickEnale:(BOOL)clickEnable
{
    [self addImages:images withPage:page];
    self.tap.enabled = clickEnable;
}
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param rect   pageControl的rect
 */
-(void)addImages:(NSArray *)images withPage:(BOOL)page pageRect:(CGRect)rect
{
    [self addImages:images withPage:page];
    self.pageControl.frame = rect;
}
/**
 *  接收图片的外部接口，可以设置自动轮播，默认时间为两秒
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 */
-(void)addImages:(NSArray *)images withPage:(BOOL)page autoShow:(BOOL)show
{
    self.autoShow = show;
    [self addImages:images withPage:page];
    if (show) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(autoShow) userInfo:nil repeats:YES];
    }
}

- (void)addImages:(NSArray *)images withPage:(BOOL)page autoShow:(BOOL)show isLeft:(BOOL)left
{
    self.left = left;
    [self addImages:images withPage:page autoShow:show];
}

- (void)addImages:(NSArray *)images withPage:(BOOL)page isUp:(BOOL)up
{
    self.up = up;
    self.left = YES;
    [self addImages:images withPage:page autoShow:NO];
}

- (void)addImages:(NSArray *)images withPage:(BOOL)page isUp:(BOOL)up isLoop:(BOOL)loop
{
    self.loop = loop;
    [self addImages:images withPage:page isUp:up];
}

- (void)addImages:(NSArray *)images withPage:(BOOL)page isUp:(BOOL)up isLoop:(BOOL)loop isTap:(BOOL)tap andTapTimes:(NSUInteger)times
{
    self.loop = loop;
    [self addImages:images withPage:page isUp:up];
    self.tap.enabled = tap;
    self.tap.numberOfTapsRequired = times;
    self.tap.numberOfTouchesRequired = 1;
}

/**
 *  如果没有开启自动轮播功能，可通过此方法开启
 */
- (void)startAutoShow
{
    if (!self.isAutoShow) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(autoShow) userInfo:nil repeats:YES];
        self.autoShow = YES;
    }
}
/**
 *  如果开启了自动轮播功能，可通过此方法关闭
 */
-(void)stopAutoShow
{
    if (self.isAutoShow) {
        [self.timer invalidate];
        self.autoShow = NO;
    }
}

#pragma mark - 内部方法
/**
 *  初始化imageView属性
 */

//- (void)imageViewUpdate
//{
//    //设置当前View
//    UIImage *image = self.images[self.selectedImage];
//    self.currentView.imageView.image = image;
//    
//    if (self.isUp) {
//        if(self.selectedImage == self.images.count - 1){
//            if (!self.isLoop) {
//                image = nil;
//            } else {
//                image = self.images[0];
//            }
//        } else {
//            image = self.images[self.selectedImage + 1];
//        }
//        self.afterView.imageView.image = image;
//        //设置前一个View
//        if (self.selectedImage == 0) {
//            if (!self.isLoop) {
//                image = nil;
//            } else {
//                image = self.images.lastObject;
//            }
//        } else {
//            image = self.images[self.selectedImage - 1];
//        }
//        self.frontView.imageView.image = image;
//    } else {
//        if (self.isLeft) {
//            //设置后一个View
//            if(self.selectedImage == self.images.count - 1) {
//                image = self.images[0];
//            } else {
//                image = self.images[self.selectedImage + 1];
//            }
//            self.frontView.imageView.image = image;
//            //设置前一个View
//            if (self.selectedImage == 0) {
//                image = self.images.lastObject;
//            } else {
//                image = self.images[self.selectedImage - 1];
//            }
//            self.afterView.imageView.image = image;
//        } else {
//            //设置后一个View
//            if(self.selectedImage == self.images.count - 1){
//                image = self.images[0];
//            } else {
//                image = self.images[self.selectedImage + 1];
//            }
//            self.afterView.imageView.image = image;
//            //设置前一个View
//            if (self.selectedImage == 0) {
//                image = self.images.lastObject;
//            } else {
//                image = self.images[self.selectedImage - 1];
//            }
//            self.frontView.imageView.image = image;
//        }
//    }
//}

/**
 *  初始化scrollView属性
 */
- (void)scrollUpdate
{
    if (self.isUp) {
        self.contentSize = CGSizeMake(self.bounds.size.width, self.bounds.size.height * 3);
        self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.contentOffset = CGPointMake(0, self.bounds.size.height);
    } else {
        self.contentSize = CGSizeMake(self.bounds.size.width * 3, self.bounds.size.height);
        self.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.contentOffset = CGPointMake(self.bounds.size.width, 0);
    }
    self.showsHorizontalScrollIndicator = NO;
    self.pagingEnabled = YES;
    self.bounces = NO;
    self.pageControl.hidden = self.isPage;
    [self bringSubviewToFront:self.pageControl];
    
}
/**
 *  刷新图片显示
 */

//- (void)refreshImages
//{
//    if (self.isUp) {
//        if (self.contentOffset.y == 0) {
//            if (self.selectedImage == 0) {
//                self.selectedImage = self.images.count - 1;
//            } else {
//                self.selectedImage -= 1;
//            }
//        } else if (self.contentOffset.y == self.bounds.size.height * 2){
//            if (self.selectedImage + 1== self.images.count) {
//                self.selectedImage = 0;
//            } else {
//                self.selectedImage += 1;
//            }
//        } else {
//            return;
//        }
//        [self setContentOffset:CGPointMake(0, self.bounds.size.height) animated:NO];
//    } else {
//        if (self.isLeft) {
//            if (self.contentOffset.x == 0) {
//                if (self.selectedImage + 1== self.images.count) {
//                    self.selectedImage = 0;
//                } else {
//                    self.selectedImage += 1;
//                }
//            } else if (self.contentOffset.x == self.bounds.size.width * 2){
//                if (self.selectedImage == 0) {
//                    self.selectedImage = self.images.count - 1;
//                } else {
//                    self.selectedImage -= 1;
//                }
//            } else {
//                return;
//            }
//        } else {
//            if (self.contentOffset.x == 0) {
//                if (self.selectedImage == 0) {
//                    self.selectedImage = self.images.count - 1;
//                } else {
//                    self.selectedImage -= 1;
//                }
//            } else if (self.contentOffset.x == self.bounds.size.width * 2){
//                if (self.selectedImage + 1== self.images.count) {
//                    self.selectedImage = 0;
//                } else {
//                    self.selectedImage += 1;
//                }
//            } else {
//                return;
//            }
//        }
//        [self setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
//    }
//    // 如果图片放大啦，让图片恢复大小
//    if (self.currentView.contentSize.height > self.currentView.frame.size.height) {
//        [self.currentView zoomToRect:self.currentView.frame animated:NO];
//    }
//
//    [self imageViewUpdate];
//    self.pageControl.currentPage = self.selectedImage;
//}
/**
 *  自动循环显示，动画效果持续时间1s
 */
- (void)autoShow
{
    [UIView animateWithDuration:0.5 animations:^{
        self.contentOffset = CGPointMake(self.frame.size.width * 2, 0);
    } completion:^(BOOL finished) {
        if (self.images.count > 0) {
            [self refreshImages];
        } else {
            [self refreshImagesWithURL];
        }
    }];
}
/**
 *  如果scrollV移除的时候timer还在工作，就移除timer
 */
-(void)dealloc
{
    if (self.isAutoShow) {
        [self.timer invalidate];
        NSLog(@"timer移除");
    }
}

- (void)gotoImageNumber:(NSUInteger)num
{
    self.selectedImage = num;
    if (self.isUp) {
        [self setContentOffset:CGPointMake(0, self.bounds.size.height) animated:NO];
    } else {
        [self setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
    }
    [self imageViewUpdate];
    self.pageControl.currentPage = self.selectedImage;
}

#pragma mark - 要搭配SDWebImage框架才能用法的外部接口

/**
 *  添加显示图片
 *
 *  @param URLArray 装有要显示图片URLStr的数组
 */
- (void)addImagesWithImageURLs:(NSArray *)URLArray
{
    self.URLArray = URLArray;
    [self scrollUpdate];
    [self imageViewUpdateWithURL];
}
/**
 *  接收显示图片的外部接口
 *
 *  @param URLArray 装有要显示图片URLStr的数组
 *  @param page   是否需要显示pageControl
 */
- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page
{
    [self addImagesWithImageURLs:URLArray];
    self.pageControl.hidden = !page;
}
/**
 *  接收显示图片的外部接口
 *
 *  @param URLArray 装有要显示图片URLStr的数组
 *  @param page   是否需要显示pageControl
 *  @param clickEnable 图片是否可以点击
 */
- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page witnImageClickEnale:(BOOL)clickEnable
{
    [self addImagesWithImageURLs:URLArray withPage:page];
    self.tap.enabled = clickEnable;
}
/**
 *  接收显示图片的外部接口
 *
 *  @param URLArray 装有要显示图片URLStr的数组
 *  @param page   是否需要显示pageControl
 *  @param rect   pageControl的rect
 */
-(void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page pageRect:(CGRect)rect
{
    [self addImagesWithImageURLs:_URLArray withPage:page];
    self.pageControl.frame = rect;
}
/**
 *  接收图片的外部接口，可以设置自动轮播，默认时间为两秒
 *
 *  @param URLArray 装有要显示图片URLStr的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 */
- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page autoShow:(BOOL)show
{
    [self addImagesWithImageURLs:URLArray withPage:page];
    if (show) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(autoShow) userInfo:nil repeats:YES];
    }
}

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page autoShow:(BOOL)show isLeft:(BOOL)left
{
    self.left = left;
    [self addImagesWithImageURLs:URLArray withPage:page autoShow:show];
}

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page isUp:(BOOL)up
{
    self.up = up;
    self.left = YES;
    [self addImagesWithImageURLs:URLArray withPage:page autoShow:NO];
}

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page isUp:(BOOL)up isLoop:(BOOL)loop
{
    self.loop = loop;
    [self addImagesWithImageURLs:URLArray withPage:page isUp:up];
}

#pragma mark - 搭配SDWebImage框架的内部方法

//- (void)imageViewUpdateWithURL
//{
//    //设置当前View
//    [self.currentView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//    
//    if (self.isUp) {
//        //设置后一个View
//        if(self.selectedImage == self.URLArray.count - 1){
//            if (!self.isLoop) {
//                self.afterView.imageView.image = nil;
//            } else {
//                [self.afterView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[0]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            }
//        } else {
//            [self.afterView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//        }
//        //设置前一个View
//        if (self.selectedImage == 0) {
//            if (!self.isLoop) {
//                self.frontView.imageView.image = nil;
//            } else {
//                [self.frontView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            }
//        } else {
//            [self.frontView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//        }
//    } else {
//        if (self.isLeft) {
//            //设置后一个View
//            if(self.selectedImage == self.URLArray.count - 1) {
//                [self.frontView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.firstObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            } else {
//                [self.frontView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            }
//            //设置前一个View
//            if (self.selectedImage == 0) {
//                [self.afterView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            } else {
//                [self.afterView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            }
//        } else {
//            //设置后一个View
//            if(self.selectedImage == self.URLArray.count - 1){
//                [self.afterView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.firstObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            } else {
//                [self.afterView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            }
//            //设置前一个View
//            if (self.selectedImage == 0) {
//                [self.frontView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            } else {
//                [self.frontView.imageView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
//            }
//        }
//    }
//}

- (void)refreshImagesWithURL;
{
    if (self.isUp) {
        if (self.contentOffset.y == 0) {
            if (self.selectedImage == 0) {
                self.selectedImage = self.URLArray.count - 1;
            } else {
                self.selectedImage -= 1;
            }
        } else if (self.contentOffset.y == self.bounds.size.height * 2){
            if (self.selectedImage + 1== self.URLArray.count) {
                self.selectedImage = 0;
            } else {
                self.selectedImage += 1;
            }
        } else {
            return;
        }
        [self setContentOffset:CGPointMake(0, self.bounds.size.height) animated:NO];
    } else {
        if (self.isLeft) {
            if (self.contentOffset.x == 0) {
                if (self.selectedImage + 1== self.URLArray.count) {
                    self.selectedImage = 0;
                } else {
                    self.selectedImage += 1;
                }
            } else if (self.contentOffset.x == self.bounds.size.width * 2){
                if (self.selectedImage == 0) {
                    self.selectedImage = self.URLArray.count - 1;
                } else {
                    self.selectedImage -= 1;
                }
            } else {
                return;
            }
        } else {
            if (self.contentOffset.x == 0) {
                if (self.selectedImage == 0) {
                    self.selectedImage = self.URLArray.count - 1;
                } else {
                    self.selectedImage -= 1;
                }
            } else if (self.contentOffset.x == self.bounds.size.width * 2){
                if (self.selectedImage + 1== self.URLArray.count) {
                    self.selectedImage = 0;
                } else {
                    self.selectedImage += 1;
                }
            } else {
                return;
            }
        }
        [self setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
    }
    [self imageViewUpdateWithURL];
    self.pageControl.currentPage = self.selectedImage;
}

#pragma mark - 点击方法

- (void)imageClick:(UIButton *)sender
{
    if (self.tap.numberOfTapsRequired == 1) {
        if ([self.SYPageScrollDelegate respondsToSelector:@selector(pageScrollView:withCurrentImagePage:)]) {
            [self.SYPageScrollDelegate pageScrollView:self withCurrentImagePage:self.selectedImage];
        }
    } else if(self.tap.numberOfTapsRequired == 2){
        NSLog(@"双击成功！！！");
    }
    
}
#pragma  mark - currentImageDelegate

-(void)imageScrollSingleClick
{
    NSLog(@"调用block方法");
    if (self.singleTapBlock) {
        self.singleTapBlock();
    }
}

#pragma mark - 不能放大缩小的版本

- (UIImageView *)frontView
{
    if (!_frontView) {
        UIImageView *frontView = [[UIImageView alloc] init];
        if (!self.isLoop) {
            frontView.backgroundColor = [UIColor grayColor];
        }
        _frontView = frontView;
        [self addSubview:self.frontView];
        if (self.isUp) {
            _frontView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        } else {
            _frontView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        }
    }
    return _frontView;
}

- (UIImageView *)currentView
{
    if (!_currentView) {
        UIImageView *currentView = [[UIImageView alloc] init];
        _currentView = currentView;
        [self addSubview:self.currentView];
        if (self.isUp) {
            _currentView.frame = CGRectMake(0, self.bounds.size.height, self.bounds.size.width, self.bounds.size.height);
        } else {
            _currentView.frame = CGRectMake(self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height);
        }
    }
    return _currentView;
}

- (UIImageView *)afterView
{
    if (!_afterView) {
        UIImageView *afterView = [[UIImageView alloc] init];
        if (!self.isLoop) {
            afterView.backgroundColor = [UIColor grayColor];
        }
        _afterView = afterView;
        [self addSubview:self.afterView];
        if (self.isUp) {
            _afterView.frame = CGRectMake(0, self.bounds.size.height * 2, self.bounds.size.width, self.bounds.size.height);
        } else {
            _afterView.frame = CGRectMake(self.bounds.size.width * 2, 0, self.bounds.size.width, self.bounds.size.height);
        }
    }
    return _afterView;
}

- (void)imageViewUpdate
{
    //设置当前View
    UIImage *image = self.images[self.selectedImage];
    self.currentView.image = image;

    if (self.isUp) {
        if(self.selectedImage == self.images.count - 1){
            if (!self.isLoop) {
                image = nil;
            } else {
                image = self.images[0];
            }
        } else {
            image = self.images[self.selectedImage + 1];
        }
        self.afterView.image = image;
        //设置前一个View
        if (self.selectedImage == 0) {
            if (!self.isLoop) {
                image = nil;
            } else {
                image = self.images.lastObject;
            }
        } else {
            image = self.images[self.selectedImage - 1];
        }
        self.frontView.image = image;
    } else {
        if (self.isLeft) {
            //设置后一个View
            if(self.selectedImage == self.images.count - 1) {
                image = self.images[0];
            } else {
                image = self.images[self.selectedImage + 1];
            }
            self.frontView.image = image;
            //设置前一个View
            if (self.selectedImage == 0) {
                image = self.images.lastObject;
            } else {
                image = self.images[self.selectedImage - 1];
            }
            self.afterView.image = image;
        } else {
            //设置后一个View
            if(self.selectedImage == self.images.count - 1){
                image = self.images[0];
            } else {
                image = self.images[self.selectedImage + 1];
            }
            self.afterView.image = image;
            //设置前一个View
            if (self.selectedImage == 0) {
                image = self.images.lastObject;
            } else {
                image = self.images[self.selectedImage - 1];
            }
            self.frontView.image = image;
        }
    }
}

- (void)refreshImages
{
    if (self.isUp) {
        if (self.contentOffset.y == 0) {
            if (self.selectedImage == 0) {
                self.selectedImage = self.images.count - 1;
            } else {
                self.selectedImage -= 1;
            }
        } else if (self.contentOffset.y == self.currentView.frame.size.height * 2){
            if (self.selectedImage + 1== self.images.count) {
                self.selectedImage = 0;
            } else {
                self.selectedImage += 1;
            }
        } else {
            return;
        }
        [self setContentOffset:CGPointMake(0, self.currentView.frame.size.height) animated:NO];
    } else {
        if (self.isLeft) {
            if (self.contentOffset.x == 0) {
                if (self.selectedImage + 1== self.images.count) {
                    self.selectedImage = 0;
                } else {
                    self.selectedImage += 1;
                }
            } else if (self.contentOffset.x == self.currentView.frame.size.width * 2){
                if (self.selectedImage == 0) {
                    self.selectedImage = self.images.count - 1;
                } else {
                    self.selectedImage -= 1;
                }
            } else {
                return;
            }
        } else {
            if (self.contentOffset.x == 0) {
                if (self.selectedImage == 0) {
                    self.selectedImage = self.images.count - 1;
                } else {
                    self.selectedImage -= 1;
                }
            } else if (self.contentOffset.x == self.currentView.frame.size.width * 2){
                if (self.selectedImage + 1== self.images.count) {
                    self.selectedImage = 0;
                } else {
                    self.selectedImage += 1;
                }
            } else {
                return;
            }
        }
        [self setContentOffset:CGPointMake(self.currentView.frame.size.width, 0) animated:NO];
    }
    [self imageViewUpdate];
    self.pageControl.currentPage = self.selectedImage;
}

- (void)imageViewUpdateWithURL
{
    //设置当前View
    [self.currentView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];

    if (self.isUp) {
        //设置后一个View
        if(self.selectedImage == self.URLArray.count - 1){
            if (!self.isLoop) {
                self.afterView.image = nil;
            } else {
                [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[0]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            }
        } else {
            [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
        }
        //设置前一个View
        if (self.selectedImage == 0) {
            if (!self.isLoop) {
                self.frontView.image = nil;
            } else {
                [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            }
        } else {
            [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
        }
    } else {
        if (self.isLeft) {
            //设置后一个View
            if(self.selectedImage == self.URLArray.count - 1) {
                [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.firstObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            } else {
                [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            }
            //设置前一个View
            if (self.selectedImage == 0) {
                [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            } else {
                [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            }
        } else {
            //设置后一个View
            if(self.selectedImage == self.URLArray.count - 1){
                [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.firstObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            } else {
                [self.afterView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage + 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            }
            //设置前一个View
            if (self.selectedImage == 0) {
                [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.URLArray.lastObject] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            } else {
                [self.frontView sd_setImageWithURL:[NSURL URLWithString:self.URLArray[self.selectedImage - 1]] placeholderImage:nil options:SDWebImageRetryFailed | SDWebImageLowPriority];
            }
        }
    }
}
@end
