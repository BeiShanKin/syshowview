//
//  ViewController.m
//  SYShowScrollDemo
//
//  Created by lz on 15/12/3.
//  Copyright © 2015年 SY. All rights reserved.
//

#import "ViewController.h"
#import "SYPageScroll.h"
#import "SYShowView.h"
#import "SYFreeShowView.h"
#import "SYSDWebShowView.h"

@interface ViewController ()
@property (strong, nonatomic) NSMutableArray *images;
@property (weak, nonatomic) IBOutlet SYFreeShowView *freeShowView;
@property (weak, nonatomic) IBOutlet SYSDWebShowView *sdwebShowView;


@end

@implementation ViewController

- (NSMutableArray *)images
{
    if (!_images) {
        _images = [NSMutableArray array];
    }
    return _images;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    for (int i = 1;i < 5;i++)
    {
        NSString *imageName = [NSString stringWithFormat:@"photo%i.jpg",i];
        UIImage *image = [UIImage imageNamed:imageName];
        [self.images addObject:image];
    }
    NSLog(@"viewDidLoad ");
    
    NSArray *URLArray =@[@"http://210.72.20.127/comic/Public/image_upload/5481524106c2e.jpg",@"http://210.72.20.127/comic/Public/image_upload/54858110b2234.jpg",@"http://210.72.20.127/comic/Public/image_upload/54856e54739d7.jpg",@"http://210.72.20.127/comic/Public/image_upload/54865ba7b886e.jpg",@"http://210.72.20.127/comic/Public/image_upload/54bc7abc8db78.jpg"];
    
    self.images = URLArray.mutableCopy;
//    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30)];
//    pageControl.backgroundColor = [UIColor redColor];
//    [self.freeShowView addImages:self.images autoShow:YES andShowInterval:2.0 showPage:YES withPageControl:pageControl];
//    [self.freeShowView addImages:self.images autoShow:YES andShowInterval:2.0 showPage:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear:(BOOL)animated");
//    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 200, self.view.bounds.size.width, 20)];
//    pageControl.numberOfPages = self.images.count;
//    [self.freeShowView addImages:self.images autoShow:YES andShowInterval:2.0 showPage:YES withPageControl:pageControl];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.sdwebShowView addGestureRecognizer:tap];
    [self.sdwebShowView addImages:self.images autoShow:YES showPage:YES];
    
}

- (void)tapClick
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(arc4random() % 100, arc4random() %200, self.view.bounds.size.width, 20)];
    label.text = @"我爱北京天安门";
    [self.freeShowView addSubview:label];
    [self.freeShowView bringSubviewToFront:label];
    NSLog(@"%@",label.text);
}

@end
