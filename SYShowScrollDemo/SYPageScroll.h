//
//  SYPageScroll.h
//  ChinaComic
//
//  Created by lz on 15/9/15.
//  Copyright (c) 2015年 SY. All rights reserved.
//  如果是通过autolayout进行布局，请在viewDidAppear方法中调用添加图片的外部接口，因为scrollView的具体宽高如果没有在SB中写死，那么只有当viewDidAppear方法被调用时autolayout才会布局好scrollView的实际frame，这个时候再调用添加图片的外部方法才能使得contentSize是适合图片轮播的。



#import <UIKit/UIKit.h>
@class SYPageScroll;
@protocol SYPageScrollDelegate <NSObject>
- (void)pageScrollView:(SYPageScroll *)pageScroll withCurrentImagePage:(NSUInteger)pageNum;
@end

@interface SYPageScroll : UIScrollView
//是否支持放大
@property (assign,nonatomic,getter=isFit) BOOL fit;
@property (assign,nonatomic,getter=isZoom) BOOL zoom;
@property (weak,nonatomic) id<SYPageScrollDelegate> SYPageScrollDelegate;
@property (strong,nonatomic) void(^singleTapBlock)();
/**
 *  当前显示页的页码
 */
@property (assign,nonatomic,readonly) NSUInteger selectedImage;
/**
 *  刷新图片的外部接口，在scrollView结束滑动的代理方法中调用
 */
- (void)refreshImages;
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有需要显示的图片的数组
 */
- (void)addImages:(NSArray *)images;
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page;
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param clickEnable 是否可以点击图片
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page witnImageClickEnale:(BOOL)clickEnable;
/**
 *  接收显示图片的外部接口
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param rect   pageControl的rect
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page pageRect:(CGRect)rect;
/**
 *  接收图片的外部接口，可以设置自动轮播
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page autoShow:(BOOL)show;
/**
 *  接收图片的外部接口，可以设置自动轮播,左滑模式
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 *  @param left   是否开启左滑模式
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page autoShow:(BOOL)show isLeft:(BOOL)left;
/**
 *  接收图片的外部接口，专门用来设置上下滑动模式的，如果不设置上下滑动，则默认为左滑模式
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 *  @param up     是否开启上下滑动模式
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page isUp:(BOOL)up;
/**
 *  接收图片的外部接口，专门用来设置上下滑动模式的，如果不设置上下滑动，则默认为左滑模式
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 *  @param up     是否开启上下滑动模式
 *  @param loop   是否开启循环模式
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page isUp:(BOOL)up isLoop:(BOOL)loop;
/**
 *  接收图片的外部接口，专门用来设置上下滑动模式的，如果不设置上下滑动，则默认为左滑模式
 *
 *  @param images 装有要显示图片的数组
 *  @param page   是否需要显示pageControl
 *  @param show   是否自动轮播
 *  @param up     是否开启上下滑动模式
 *  @param loop   是否开启循环模式
 *  @param tap   是否开启可点击模式
 *  @param times   点击几次响应
 */
- (void)addImages:(NSArray *)images withPage:(BOOL)page isUp:(BOOL)up isLoop:(BOOL)loop isTap:(BOOL)tap andTapTimes:(NSUInteger)times;
/**
 *  如果没有开启自动轮播功能，可通过此方法开启
 */
- (void)startAutoShow;
/**
 *  如果开启了自动轮播功能，可通过此方法关闭
 */
- (void)stopAutoShow;
/**
 *  页面跳转方法，可以直接跳转到任意想要到的页面
 *
 *  @param num 想要跳转的页面的页数
 */
- (void)gotoImageNumber:(NSUInteger)num;
/**
 *  通过URLStr来加载图片,要搭配SDWebImages才能使用
 *
 *  @param URLArray 装有URLStr的数组
 */
- (void)addImagesWithImageURLs:(NSArray *)URLArray;

- (void)imageViewUpdateWithURL;

- (void)refreshImagesWithURL;

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page;

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page witnImageClickEnale:(BOOL)clickEnable;

-(void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page pageRect:(CGRect)rect;

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page autoShow:(BOOL)show;

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page autoShow:(BOOL)show isLeft:(BOOL)left;

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page isUp:(BOOL)up;

- (void)addImagesWithImageURLs:(NSArray *)URLArray withPage:(BOOL)page isUp:(BOOL)up isLoop:(BOOL)loop;
@end
